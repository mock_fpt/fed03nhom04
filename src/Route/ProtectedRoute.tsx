import { Navigate, Outlet } from 'react-router-dom';
import { eProtectedRouteProps } from '../Model/eProtectedRouteProps'

const ProtectedRoute = ({ isAuthenticated, authenticationPath, children }: eProtectedRouteProps) => {
  if (!isAuthenticated) {
    return <Navigate to={authenticationPath} replace />;
  }

  return children ? children : <Outlet />;
};

export default ProtectedRoute


