import { BrowserRouter, Link, Navigate, Outlet, Route, Routes } from 'react-router-dom'
import Home from './Components/Home/Home'
import Register from './Components/Register/Register';
import Login from './Components/Login/Login';
import './common.scss'
import './common1.css'
import MovieShowing from './Components/MovieShowing/MovieShowing';
import MovieComingSoon from './Components/MovieComingSoon/MovieComingSoon';
import MovieDetail from './Components/MovieDetail/MovieDetail';
import ProtectedRoute from './Route/ProtectedRoute';
import { eProtectedRouteProps } from './Model/eProtectedRouteProps';
import Profile from './Components/Profile/Profile';
import { useAppSelector } from './Redux/stores/hooks';
import { RootState } from './Redux/stores/Store';
import { getCookie } from './Redux/reducers/User/user.actions';
import UpgradeSubscription from './Components/Profile/UpgradeSubscription';
import CreditCard from './Components/CreditCard/CreditCard';
import Schedule from './Components/Schedule/Schedule';
import Ticket from './Components/Ticket/Ticket';

export default function App() {
  const { loggedIn } = useAppSelector((state: RootState) => state.rdcUser);
  const userCookie = getCookie('movie');
  const defaultProtectedRouteProps: Omit<eProtectedRouteProps, 'children'> = {
    isAuthenticated: loggedIn || !!userCookie,
    authenticationPath: '/home',
  };


  return (
    <>
      <BrowserRouter>

        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='register' element={<Register />} />
          <Route path='login' element={<Login />} />
          <Route path='movieshowing' element={<MovieShowing />} />
          <Route path='moviecomingsoon' element={<MovieComingSoon />} />
          <Route path='moviedetail/:id' element={<MovieDetail />} />
          <Route path='ticket/:cinemaId/:sessonId/:filmId/:cinemaCode/:screenName/:showDate/:showTime' element={<Ticket />} />
          <Route path='schedule' element={<Schedule />} />
          <Route index element={<Home />} />
          <Route path="home" element={<Home />} />

          <Route path="register" element={<Register />} />
          <Route path="login" element={<Login />} />
          <Route path='/profile' element={<ProtectedRoute {...defaultProtectedRouteProps} children={<Profile />} />} />
          <Route path='/profile/upgrade' element={<ProtectedRoute {...defaultProtectedRouteProps} children={<UpgradeSubscription />} />} />
          <Route path='/creditcard' element={<ProtectedRoute {...defaultProtectedRouteProps} children={<CreditCard />} />} />


          <Route path="*" element={<p>There's nothing here: 404!</p>} />
        </Routes>
      </BrowserRouter>
    </>
  )
}
