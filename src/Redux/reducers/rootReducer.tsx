
import { combineReducers } from 'redux';
import rdcCreditCard from './CreditCard/credit.reducer';
import rdcSubscription from './Payment/payment.reducer';
import rdcUser from './User/user.reducer';

const rootReducer = combineReducers({
  rdcUser: rdcUser,
  rdcCreditCard: rdcCreditCard,
  rdcSubscription:rdcSubscription
});

export default rootReducer;