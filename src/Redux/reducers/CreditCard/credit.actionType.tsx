export const creditCardConstants = {
    CHECK_BANK_CARD_SUCCESS: 'CHECK_BANK_CARD_SUCCESS',
    CHECK_BANK_CARD_FAILURE: 'CHECK_BANK_CARD_FAILURE',
    UPDATE_BALANCE: 'UPDATE_BALANCE',
};

export type BankCardType = {
    "BankId": number;
    "CardNumber": string;
    "CardName": string,
    "ExpireDate": string,
    "CVV": string
}

export type BankInfoType = {
    "BankId": number;
    "CardNumber": string;
    "Email": string;
    "CardName": string;
    "ExpireDate": string;
    "CVV": string;
    "Balance": number,
    "CreateBy": string;
}

export type BankCardUpdateType = {
    "BankId": number;
    "CardNumber": string;
    "CardName": string,
    "ExpireDate": string,
    "CVV": string
    "Balance": number;
}

export interface CheckBankAction {
    type: typeof creditCardConstants.CHECK_BANK_CARD_SUCCESS;
    payload: BankInfoType
}


export interface UpdateBalanceAction {
    type: typeof creditCardConstants.UPDATE_BALANCE;
    payload: BankCardUpdateType
}
