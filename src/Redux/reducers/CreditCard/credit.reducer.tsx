import { BankCardType, BankInfoType, CheckBankAction, creditCardConstants } from "./credit.actionType";



export interface IDefaultState {
    creditCard: BankInfoType,
    checkCard: boolean
}
export const defaultState: IDefaultState = {
    creditCard: {
        "BankId": 1,
        "CardNumber": "",
        "Email": "",
        "CardName": "",
        "ExpireDate": "",
        "CVV": "",
        "Balance": 0,
        "CreateBy": ''
    },
    checkCard: false
};


const rdcCreditCard = (
    state: IDefaultState = defaultState,
    action: CheckBankAction
): IDefaultState => {
    switch (action.type) {
        case creditCardConstants.CHECK_BANK_CARD_SUCCESS:
            return {
                ...state, creditCard: action.payload, checkCard: true
            };
        case creditCardConstants.CHECK_BANK_CARD_SUCCESS:
            return {
                ...state, checkCard: false
            };
        default:
            return state;
    }
};

export default rdcCreditCard;