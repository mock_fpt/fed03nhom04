import { Dispatch } from "react";
import { BankCardType, BankCardUpdateType, CheckBankAction, creditCardConstants, UpdateBalanceAction } from "./credit.actionType"

export const actionCheckCard = (card: BankCardType) => {

    return async (dispatch: Dispatch<CheckBankAction>) => {
        try {
            const result = await checkBankCard(card);
            console.log(result);


            if (result) {
                dispatch({
                    type: creditCardConstants.CHECK_BANK_CARD_SUCCESS,
                    payload: result
                })
            }
            else {
                alert('Payment Error. Please try again')
                dispatch({
                    type: creditCardConstants.CHECK_BANK_CARD_FAILURE,
                    payload: result
                });
            }
        } catch (error) {
            console.log(error);
        }
    };
};


export const actionUpdateBalance = (card: BankCardUpdateType) => {

    return async (dispatch: Dispatch<UpdateBalanceAction>) => {
        try {
            const result = await updateBalance(card);

            if (result == 'Success') {
                alert('Payment Success!')
            }
            else alert('Payment Error')
        } catch (error) {
            console.log(error);
        }
    };
};

const checkBankCard = async (card: BankCardType) => {

    const requestOptions = {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(card)
    };

    try {
        return await fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/Bank/BankCard', requestOptions)
            .then(res => res.json())
            .then(data => {
                return data
            })
    } catch (error) {
        console.log("error", error);
    }
}


const updateBalance = async (card: BankCardUpdateType) => {

    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(card),
    };

    let url = 'https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/Bank/BankCard';

    try {
        return await fetch(url, requestOptions)
            .then(res => res.text())
    } catch (error) {
        console.log("error", error);
    }
}