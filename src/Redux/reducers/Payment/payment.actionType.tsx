export const subscriptionConstants = {
    SET_SUBSCRIPTION: 'SET_SUBSCRIPTION',
};

export type PlanType = {
    type: string,
    method: string,
    amount: number
}



export interface SubscriptionAction {
    type: typeof subscriptionConstants.SET_SUBSCRIPTION;
    payload: PlanType
}


