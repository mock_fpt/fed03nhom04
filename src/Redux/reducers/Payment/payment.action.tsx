import { Dispatch } from "react";
import { PlanType, SubscriptionAction, subscriptionConstants } from "./payment.actionType";

export const actionSetSubscription = (plan: PlanType) => {

    return async (dispatch: Dispatch<SubscriptionAction>) => {
        try {
            dispatch({
                type: subscriptionConstants.SET_SUBSCRIPTION,
                payload: plan
            })
        } catch (error) {
            console.log(error);
        }
    };
};


