import { getCookie } from "../User/user.actions";
import { PlanType, SubscriptionAction, subscriptionConstants } from "./payment.actionType";



export interface IDefaultState {
    payment: PlanType
}

let subPlan = {
    type: '',
    method: '',
    amount: 0
};
let paymentCookie = getCookie('subscriptionPlan');
if (paymentCookie != '') {
    subPlan = JSON.parse(paymentCookie);
}

const defaultState: IDefaultState = {
    payment: subPlan
}

const rdcSubscription = (
    state: IDefaultState = defaultState,
    action: SubscriptionAction
): IDefaultState => {
    switch (action.type) {
        case subscriptionConstants.SET_SUBSCRIPTION:
            console.log(action.payload);

            return {
                ...state, payment: action.payload
            };
        default:
            return state;
    }
};

export default rdcSubscription;