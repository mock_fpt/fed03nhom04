import { getCookie } from "./user.actions";
import { userConstants, UserLoginDispatchTypes, UserLoginType, UserRegisterType } from "./user.actionTypes";

let userCookie = getCookie('movie');
let user = {
  Email: '',
  Password: ''
};

if (userCookie != '') {
  user = JSON.parse(userCookie);
}
let userRegister = {};


export interface IDefaultState {
  loggedIn?: boolean;
  user?: UserLoginType ;
  userRegister?: UserRegisterType | {};
}
const defaultState: IDefaultState = { loggedIn: false, user, userRegister };


const rdcUser = (
  state: IDefaultState = defaultState,
  action: UserLoginDispatchTypes
): IDefaultState => {
  switch (action.type) {

    case userConstants.LOGIN_SUCCESS:
      return {
        ...state, loggedIn: true,
        user: action.payload
      };
    case userConstants.LOGIN_FAILURE:
      return {
        ...state, loggedIn: false
      };
    case userConstants.LOGOUT:
      return {
        ...state, loggedIn: false
      };

    case userConstants.DELETE_USER:
      return {
        ...state, loggedIn: false
      };

    case userConstants.REGISTER_SUCCESS:
      return {
        ...state, loggedIn: true,
        userRegister: action.payload
      };
    case userConstants.REGISTER_FAILURE:
      return {};

    default:
      return state;
  }
};

export default rdcUser;

