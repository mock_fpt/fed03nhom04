import { Dispatch } from 'redux';
import { DeleteUserAction, userConstants, UserLoginDispatchTypes, UserLoginType, UserLogoutDispatchType, UserRegisterDispatchTypes, UserRegisterType, UserUpdateType } from './user.actionTypes';



export const actionLogin = (user: UserLoginType) => {

    return async (dispatch: Dispatch<UserLoginDispatchTypes>) => {
        try {
            const result = await login(user);
            // console.log(result);
            if (result?.status == 200) {
                dispatch({
                    type: userConstants.LOGIN_SUCCESS,
                    payload: user
                });

            }
            else alert('Email or password is incorrect. Please try again')
        } catch (error) {
            dispatch({
                type: userConstants.LOGIN_FAILURE,
                payload: user
            });

            console.log(error);
        }
    };
};

export const actionLogout = () => {
    document.cookie = "movie=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

    return (dispatch: Dispatch<UserLogoutDispatchType>) => {
        dispatch({
            type: userConstants.LOGOUT
        });
    }
}


export const actionRegister = (user: UserRegisterType) => {

    return async (dispatch: Dispatch<UserRegisterDispatchTypes>) => {
        try {
            const result = await register(user);
            if (result?.status == 200) {
                dispatch({
                    type: userConstants.REGISTER_SUCCESS,
                    payload: user
                });
                alert('Registration successful')
            }
            else alert('User already exists. Please try with another one.')
        } catch (error) {
            dispatch({
                type: userConstants.REGISTER_FAILURE,
                payload: user
            });
            console.log(error);
        }
    };
};

export const actionUpdateUser = async (user: UserUpdateType) => {
    try {
        const result = await updateUser(user);
        console.log(result);

        if (result?.status == 200) {
            alert('Profile updated successfully')
        }
        else alert('Please try again.')
    } catch (error) {
        console.log(error);
    }
};


export const actionDeleteUser = (email: string) => {

    return async (dispatch: Dispatch<DeleteUserAction>) => {
        try {
            const result = await deleteUser(email);
            if (result?.status == 200) {
                dispatch({
                    type: userConstants.DELETE_USER
                });
                alert('Delete successful')

            }
        } catch (error) {
            console.log(error);
        }
    };
};

export const deleteUser = async (email: string) => {
    try {
        const url = "https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/user"
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ "Email": email })
        };

        let responses = await fetch(url, requestOptions)
        return responses;
    } catch (error) {
        console.log(error);
    }
}

const login = async (user: UserLoginType) => {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    };

    try {
        let responses = await fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/Login', requestOptions)
        return responses;
    } catch (error) {
        console.log("error", error);
    }
}

export const register = async (user: UserRegisterType) => {

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    };

    try {
        let responses = await fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/user', requestOptions)
        return responses;
    } catch (error) {
        console.log("error", error);
    }
}

export const updateUser = async (user: UserUpdateType) => {

    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    };

    try {
        let responses = await fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/user', requestOptions)
        return responses;
    } catch (error) {
        console.log("error", error);
    }
}

export const getAll = async () => {

    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    };

    try {
        return await fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/user', requestOptions)
            .then(res => res.json())
            .then(data => {
                return data
            })
    } catch (error) {
        console.log("error", error);
    }
}



export const cookieCreator = (cookieName: string, cookieValue: string, minutesToExpire: number) => {
    let date = new Date();
    date.setTime(date.getTime() + minutesToExpire * 1000);
    document.cookie =
        cookieName + " = " + cookieValue + "; expires = " + date.toDateString();
};

export const getCookie = (name: string) => {
    let match = document.cookie.match(new RegExp("(^| )" + name + "=([^;]+)"));
    return match ? match[2] : "";
};

export const deleteCookie = (name: string) => {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};