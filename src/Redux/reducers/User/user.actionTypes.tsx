export const userConstants = {
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',

    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',

    LOGOUT: 'USERS_LOGOUT',

    GETALL_SUCCESS: 'USERS_GETALL_SUCCESS',
    GETALL_FAILURE: 'USERS_GETALL_FAILURE',

    DELETE_USER: 'DELETE_USER',
};

export type UserItemType = {
    Email: string;
    Name: string;
    Role: number;
}

export type UserLoginType = {
    Email: string;
    Password: string;
}

export type UserRegisterType = {
    Email: string;
    Name: string;
    Password: string;
    Role: number;
}

export type UserUpdateType = {
    Email: string;
    Name: string;
    Password: string;
}

export interface LoginSuccess {
    type: typeof userConstants.LOGIN_SUCCESS;
    payload: UserLoginType
}

export interface LoginFailure {
    type: typeof userConstants.GETALL_FAILURE;
    payload: UserLoginType
}

export interface RegisterSuccess {
    type: typeof userConstants.REGISTER_SUCCESS;
    payload: UserRegisterType
}

export interface RegisterFailure {
    type: typeof userConstants.REGISTER_FAILURE;
    payload: UserRegisterType
}

export interface Logout {
    type: typeof userConstants.LOGOUT;
}

export interface DeleteUserAction {
    type: typeof userConstants.DELETE_USER;
}

export type UserLoginDispatchTypes = LoginSuccess | LoginFailure;

export type UserRegisterDispatchTypes = RegisterSuccess | RegisterFailure;

export type UserLogoutDispatchType = Logout;