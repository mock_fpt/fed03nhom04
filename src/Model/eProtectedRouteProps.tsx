export interface eProtectedRouteProps {
    isAuthenticated: boolean,
    authenticationPath: string,
    children: JSX.Element
}