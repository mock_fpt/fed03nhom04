import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { eUserRegister } from '../../Model/eUser';
import { actionSetSubscription } from '../../Redux/reducers/Payment/payment.action';
import { actionRegister, cookieCreator, register } from '../../Redux/reducers/User/user.actions';
import { UserRegisterType } from '../../Redux/reducers/User/user.actionTypes';
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks';
import { RootState } from '../../Redux/stores/Store';

import './Register.scss'

type Props = {}

type Option = {
    id: number;
    label: string;
    isChecked: boolean;
}

function Register({ }: Props) {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const { loggedIn } = useAppSelector((state: RootState) => state.rdcUser);
    useEffect(() => {
        if (loggedIn) {
            navigate('/profile')
        }
    }, [loggedIn]);



    const option1 = [
        { id: 0, label: "Premium -$39 / 3 Months with a 5 days free trial" },
        { id: 1, label: "Basic -$19 / 1 Month" },
        { id: 2, label: "Free" }
    ];
    const option2 = [
        { id: 0, label: "Manual / Offline" },
        { id: 1, label: "Credit Card" },
    ];

    const [checkState1, setCheckState1] = useState<number>(2)
    const [checkState2, setCheckState2] = useState<number>(0)


    const checkList = [
        {
            id: 'userName',
            idError: 'userNameError',
            regex: /^.+$/,
            messEmpty: 'The User Name should not be blank.',
            messError: ''
        },
        {
            id: 'firstName',
            idError: 'firstNameError',
            regex: /^.+$/,
            messEmpty: 'The First Name should not be blank.',
            messError: ''
        },
        {
            id: 'lastName',
            idError: 'lastNameError',
            regex: /^.+$/,
            messEmpty: 'The Last Name should not be blank.',
            messError: ''
        },
        {
            id: 'email',
            idError: 'emailError',
            regex: /^\w+@\w+(\.\w+)+$/,
            messEmpty: 'The Email should not be blank.',
            messError: 'Please enter an e-mail address.'
        },
        {
            id: 'password',
            idError: 'passWordError',
            regex: /^.+$/,
            messEmpty: 'Please enter a password.',
            messError: ''
        },
        {
            id: 'confirmPassword',
            idError: 'confirmPasswordError',
            regex: /^.+$/,
            messEmpty: 'Please repeat the password.',
            messError: 'The passwords did not match.'
        },

    ]



    const [registerData, setRegisterData] = useState<eUserRegister>({
        userName: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    })


    const [errorMsg, setErrorMsg] = useState<eUserRegister>({
        userName: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    })



    const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        // console.log(registerData);
        for (const item of checkList) {
            const text = registerData[item.id as keyof eUserRegister];
            errorMsg[item.id as keyof eUserRegister] = '';
            if (text == '') {
                setErrorMsg({ ...errorMsg, [item.id]: item.messEmpty })
                return;
            }
            if (item.regex.test(text) == false) {
                setErrorMsg({ ...errorMsg, [item.id]: item.messError })
                return;
            }
            if (item.id == 'confirmPassword' && registerData['password'] != registerData['confirmPassword']) {
                setErrorMsg({ ...errorMsg, [item.id]: item.messError })
                return;
            }
            setErrorMsg({ ...errorMsg, confirmPassword: '' })
        }

        let userInfo = {
            Email: registerData.email,
            Name: registerData.userName,
            Password: registerData.password,
            Role: 1
        }
        console.log(userInfo);

        dispatch(actionRegister(userInfo))

        let subscriptionPlan = {
            type: checkState1 == 0 ? 'Premium' : 'Basic',
            method: checkState2 == 0 ? 'Offline' : 'Credit',
            amount: checkState1 == 2 ? 0 : checkState1 == 0 ? 39 : 19
        }
        console.log(subscriptionPlan);
        cookieCreator('subscriptionPlan', JSON.stringify(subscriptionPlan), 1440)
        dispatch(actionSetSubscription(subscriptionPlan))

    }

    const handleChangeInput = (e: React.FormEvent<HTMLInputElement>): void => {
        setRegisterData({ ...registerData, [e.currentTarget.name]: e.currentTarget.value })
    }



    return (
        <div className='register'>
            <form onSubmit={(e) => handleSubmit(e)}>
                <div className='registerItem'>
                    <div>
                        <p>Username*</p>
                        <input name='userName' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.userName && <p className='errorMsg'>{errorMsg.userName}</p>}
                    </div>
                    <div>
                        <p>Email*</p>
                        <input name='email' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.email && <p className='errorMsg'>{errorMsg.email}</p>}
                    </div>
                </div>
                <div className='registerItem'>
                    <div>
                        <p>First Name</p>
                        <input name='firstName' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.firstName && <p className='errorMsg'>{errorMsg.firstName}</p>}
                    </div>
                    <div>
                        <p>Last Name</p>
                        <input name='lastName' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.lastName && <p className='errorMsg'>{errorMsg.lastName}</p>}
                    </div>
                </div>
                <div className='registerItem'>
                    <div>
                        <p>Password *</p>
                        <input type={'password'} name='password' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.password && <p className='errorMsg'>{errorMsg.password}</p>}
                    </div>
                    <div>
                        <p>Repeat Password *</p>
                        <input type={'password'} name='confirmPassword' onChange={(e) => handleChangeInput(e)} required />
                        {errorMsg.confirmPassword && <p className='errorMsg'>{errorMsg.confirmPassword}</p>}
                    </div>
                </div>
                <div className='registerPlan'>
                    {option1.map((n, i) => {
                        return <div key={i} className='registerPlanItem' onClick={() => setCheckState1(i)}>
                            <input type={'checkbox'} checked={checkState1 == n.id} onChange={() => setCheckState1(i)} />
                            <label>{n.label}</label>
                        </div>
                    })}

                    {
                        checkState1 !== 2 && <div>
                            <h3>Select a Payment Method</h3>
                            <div className='flex'>
                                {
                                    option2.map((n, i) => {
                                        return <div key={i} className='registerPlanItem mr-50' onClick={() => setCheckState2(i)}>
                                            <input type="checkbox" checked={checkState2 == n.id} onChange={() => setCheckState2(i)} />
                                            <label>{n.label}</label>
                                        </div>
                                    })
                                }
                            </div>
                            <h3>Discount Code:</h3>
                            <div className='registerPlanItem'>
                                <input className='discountInput' placeholder='Enter discount' />
                                <button className='discountBtn'>Apply</button>
                            </div>
                        </div>
                    }

                </div>
                <button className='registerBtn'>Register</button>
            </form>
        </div>
    )
}

export default Register;