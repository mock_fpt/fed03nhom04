import React from "react";
import './Footer.scss'

export default function Footer() {
    return (
        <div className="foot">
            <div className="footBox">
                <div>
                    <h3>Download Our App</h3>
                    <p><img width={150} src="http://flixgo.volkovdesign.com/main/img/Download_on_the_App_Store_Badge.svg" alt="" /></p>
                    <p><img width={150} src="http://flixgo.volkovdesign.com/main/img/google-play-badge.png" alt="" /></p>
                </div>
                <div>
                    <h3>Resources</h3>
                    <p>About Us</p>
                    <p>Pricing Plan</p>
                    <p>Help Center</p>
                </div>
                <div>
                    <h3>Legal</h3>
                    <p>Terms of Use</p>
                    <p>Privacy Policy</p>
                    <p>Security</p>
                </div>
                <div>
                    <h3>Contact</h3>
                    <p>12344567</p>
                    <p>abc@gmail.com</p>
                    <p>fb ins twt </p>
                </div>
            </div>
            <div className="footCopyright">
                <p>© FlixGo, 2018—2022. Create by Dmitry Volkov.</p>
                <div>
                    <p>Terms of Use</p>
                    <p>Privacy Policy</p>
                </div>
            </div>
        </div>
    )
}
