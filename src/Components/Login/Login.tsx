import React, { useEffect, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks';
import { UserLoginType } from '../../Redux/reducers/User/user.actionTypes';
import './Login.scss'
import { actionLogin, actionLogout, cookieCreator, getCookie } from '../../Redux/reducers/User/user.actions';
import { useNavigate } from 'react-router-dom';
import { RootState } from '../../Redux/stores/Store';


type Props = {}

function Login({ }: Props) {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const { loggedIn } = useAppSelector((state: RootState) => state.rdcUser);
    let userCookie = getCookie('movie');
    let user = {};
    if (userCookie != '') {
        user = JSON.parse(userCookie);
    }
    useEffect(() => {
        if (loggedIn || !!userCookie) {
            navigate('/profile')
        }
    }, [loggedIn]);

    const [loginInfo, setLoginInfo] = useState<UserLoginType>(
        {
            Email: '',
            Password: ''
        }
    )


    const [errorMsg, setErrorMsg] = useState<UserLoginType>({
        Email: '',
        Password: ''
    })

    const checkList = [
        {
            id: 'Email',
            idError: 'emailError',
            regex: /^\w+@\w+(\.\w+)+$/,
            messEmpty: 'The Email should not be blank.',
            messError: 'Please enter an e-mail address.'
        },
        {
            id: 'Password',
            idError: 'passWordError',
            regex: /^.+$/,
            messEmpty: 'Please enter a password.',
            messError: ''
        }

    ]

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        for (const item of checkList) {
            const text = loginInfo[item.id as keyof UserLoginType];
            errorMsg[item.id as keyof UserLoginType] = '';
            if (text == '') {
                setErrorMsg({ ...errorMsg, [item.id]: item.messEmpty })
                return;
            }
            if (item.regex.test(text) == false) {
                setErrorMsg({ ...errorMsg, [item.id]: item.messError })
                return;
            }
        }
        // console.log(loginInfo);
        const optiona = {
            method: 'POST', // or 'PUT'
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(loginInfo)
        }
        fetch('https://vietcpq.name.vn/U2FsdGVkX1+u8jfXSHG943wmff2Nw1y7uxQWzhSl5do=/user/Login', optiona)
            .then((response) => response.json())
            .then(data => {
                console.log(data)
                document.cookie = JSON.stringify(data)
            }).catch((error) => {
                console.error('Error:', error);
            });

        console.log(`${document.cookie}`);


        dispatch(actionLogin(loginInfo))
    }

    const handleChangeInput = (e: React.FormEvent<HTMLInputElement>): void => {
        setLoginInfo({ ...loginInfo, [e.currentTarget.name]: e.currentTarget.value })
    }

    const handleCheck = (e: React.FormEvent<HTMLInputElement>): void => {
        if (e.currentTarget.checked) {
            cookieCreator('movie', JSON.stringify(loginInfo), 1440)
        }
    }



    return (
        <div className='login'>
            <form onSubmit={e => handleSubmit(e)}>
                <div className='loginItem'>
                    <p>Email Address</p>
                    <input name='Email' onChange={(e) => handleChangeInput(e)} />
                    {errorMsg.Email && <p className='errorMsg'>{errorMsg.Email}</p>}
                </div>
                <div className='loginItem'>
                    <p>Password</p>
                    <input name='Password' onChange={(e) => handleChangeInput(e)} />
                    {errorMsg.Password && <p className='errorMsg'>{errorMsg.Password}</p>}
                </div>
                <div className='loginItemCheck'>
                    <input type="checkbox" onChange={(e) => handleCheck(e)} />
                    <label>Remember Me</label>
                </div>
                <button className='loginBtn'>Login</button>
                <div className='loginItemRegister'>
                    <a href='/register' className='registerLink'>Register</a>
                </div>

            </form>
        </div>
    )
}


export default Login;