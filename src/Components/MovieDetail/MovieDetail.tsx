import moment from "moment";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import './MovieDetail.scss'
import ReactPlayer from "react-player";
import HeaderNoTrans from "../Header/HeaderNoTrans";

export default function MovieDetail() {

    const { id } = useParams()
    const nav = useNavigate()
    const [Film, setFilm] = useState<any>(null)
    const [Cinemas, setCinemas] = useState<any>(null)
    const [City, setCity] = useState<any>(null)
    const [CurrentCity, setCurrentCity] = useState<any>(null)
    const [CurrentCinema, setCurrentCinema] = useState<any>(null)
    const [Overlay, setOverlay] = useState(false)
    const [Schedule, setSchedule] = useState<any>([])
    const [Trailer, setTrailer] = useState<any>(null)
    const Round = (num: number) => {
        return Math.round(num * 100) / 100
    }
    const getDate = (date: any) => {
        return moment.utc(date).format('MM/DD/YY')
    }
    useEffect(() => {
        console.log(id);

        let currentMovie: any = null
        fetch("https://teachingserver.onrender.com/cinema/nowAndSoon")
            .then(res => res.json())
            .then(data => {
                currentMovie = [
                    ...data.movieShowing.filter((n: any) => n.id === id),
                    ...data.movieCommingSoon.filter((n: any) => n.id === id)
                ][0];

                return fetch(`https://www.youtube.com/oembed?url=${currentMovie?.trailer}&format=json`)
                    .then(res => res.json())
                    .then(data => {
                        currentMovie.trailer = data.html
                    })
            })
            .then(() => setFilm(currentMovie))

        fetch('https://teachingserver.onrender.com/cinema/movie/' + id)
            .then(res => res.json())
            .then(data => setSchedule(data))

        fetch(`https://teachingserver.onrender.com/cinema/cinemas`)
            .then(res => res.json())
            .then(data => {
                setCinemas(data)
            })

        fetch(`https://teachingserver.onrender.com/cinema/city`)
            .then(res => res.json())
            .then(data => setCity(data))
    }, [id])

    const ToTicket = (cid: any, sid: any, cinemaCode: any, screenName: any, showDate: any, showTime: any) => {
        nav('/ticket/' + cid + "/" + sid + '/' + id + '/' + cinemaCode + '/' + screenName + '/' + showDate.replaceAll('/', '%2F') + '/' + showTime)
    }

    return (
        <>
            <HeaderNoTrans />
            {Film && <div className="film" >
                <div className="nav">
                    <span>Home</span>
                    <span className="ml-10 mr-10">{'>'}</span>
                    <span>{Film.name}</span>
                </div>
                <div className="movieDetail">
                    <div className="topInfo">
                        <div className="topLeft">
                            <img src={Film.imagePortrait} alt="" />
                        </div>
                        <div className="topRight">
                            <h1>{Film.name}</h1>
                            <p>Points: {Round(Film.point)}/10 of {Film.totalVotes} total votes</p>
                            <p>Age: {Film.age}</p>
                            <p>Duration: {Film.duration}</p>
                            <p>Views: {Film.views}</p>
                            <p>Start date: {getDate(Film.startdate)}</p>
                            <p>End date: {getDate(Film.enddate)}</p>
                            <button onClick={() => setOverlay(true)}>Watch trailer</button>
                            <button>Buy Ticket</button>
                        </div>
                    </div>
                    <div className="botInfo">
                        <h2>Movie description</h2>
                        <div dangerouslySetInnerHTML={{ __html: Film.description }}></div>
                    </div>
                </div>
                <div className="schedule">

                </div>
            </div>}
            <div className="scheduleTab">
                <h1>Schedule</h1>
                <div className="showtime">
                    <div className="scheduleLabel">
                        <select onChange={(event) => setCurrentCity(event.target.value)} name="" id="">
                            <option value="all"> Cities / Provinces</option>
                            {
                                City?.map((c: any, index: number) => {
                                    return <option value={c.id} key={index}>{c.name}</option>
                                })
                            }
                        </select></div>
                    <div className="scheduleLabel">
                        {/* DatePicker */}
                    </div>
                    <div className="scheduleLabel">
                        <select name="" id="" onChange={(event) => setCurrentCinema(event.target.value)}>
                            <option value="all">Cinemas</option>
                            {
                                Cinemas?.filter((c: any) => c.cityId === CurrentCity).map((c: any, index: number) => {
                                    return <option value={c.id} key={index}>{c.vistaName}</option>
                                })
                            }
                        </select>
                    </div>
                </div>
                <div className="detailBooking">
                    {console.log(CurrentCity)}
                    {/* {filter((c: any) => c.cityId === CurrentCity).}             */}
                    {
                        Schedule.filter((c: any) => { if (CurrentCity === 'all' || CurrentCity === null) return true; else return c.cityId === CurrentCity }).filter((c: any) => { if (CurrentCinema === 'all' || CurrentCinema === null) return true; else return c.id === CurrentCinema }).map((s: any, si: number) => {
                            return <div className="detailTab" key={si}>
                                <div className="cinemaTitle">
                                    {s.vistaName}
                                </div>
                                <div className="timeDetail">
                                    {
                                        s.dates.map((d: any, di: number) => {
                                            return <div className="showDate" key={di}>
                                                <h4>- {d.showDate}</h4>
                                                <div className="version">
                                                    {
                                                        d.bundles.map((b: any, bi: number) => {
                                                            return <div className="ticketVer" key={bi}>
                                                                <h5>Ticket {b.version}</h5>
                                                                <div className="showTime">
                                                                    {
                                                                        b.sessions.map((t: any, ti: number) => {
                                                                            return <button onClick={() => ToTicket(t.cinemaId, t.sessionId, s.code, t.screenName, t.showDate, t.showTime)} key={ti}>{t.showTime}</button>
                                                                        })
                                                                    }
                                                                </div>
                                                            </div>
                                                        })
                                                    }
                                                </div>
                                            </div>
                                        })
                                    }
                                </div>
                            </div>
                        })
                    }
                </div>
            </div>
            <Footer />
            {Film && <div className="trailerModal" onClick={() => setOverlay(false)} style={{ display: (Overlay ? "block" : "none") }}>
                <div className="middleBox">
                    {/* {Film.trailer} */}
                    <div dangerouslySetInnerHTML={{ __html: Film.trailer }}></div>
                    {/* <ReactPlayer url={Film.trailer} frameBorder="0"></ReactPlayer> */}
                    {/* <iframe src={Film.trailer} frameBorder="0"></iframe> */}
                </div>
            </div>}
        </>
    )
}
