import React, { useState, useEffect } from "react";
import './Schedule.scss'
import Footer from "../Footer/Footer";
import HeaderNoTrans from "../Header/HeaderNoTrans";
import { useNavigate } from "react-router-dom";

export default function Schedule() {
    const [Show, setShow] = useState(true)
    const [Active, setActive] = useState(false)
    const [Film, setFilm] = useState<any>(null)
    const [FilmId, setFilmId] = useState<any>(null)
    const [IndexMovie, setIndexMovie] = useState<any>(null)
    const [IndexCinema, setIndexCinema] = useState<any>(null)
    const [IndexMovie1, setIndexMovie1] = useState<any>(null)
    const [IndexCinema1, setIndexCinema1] = useState<any>(null)
    const [Cinemas, setCinemas] = useState<any>(null)
    const [CurrentCinema, setCurrentCinema] = useState<any>(null)
    const [CinemaId, setCinemaId] = useState<any>(null)
    const [Schedule, setSchedule] = useState<any>(null)
    const nav = useNavigate();

    useEffect(() => {
        fetch('https://teachingserver.onrender.com/cinema/nowAndSoon')
            .then(res => res.json())
            .then(data => {
                setFilm(data)
            })

        fetch(`https://teachingserver.onrender.com/cinema/cinemas`)
            .then(res => res.json())
            .then(data => {
                setCinemas(data)
            })
    }, []);

    async function HandleFilm(id: any) {
        let res = await fetch('https://teachingserver.onrender.com/cinema/movie/' + id)
        let data = await res.json()
        return setSchedule(data)
    }

    async function HandleCinemaId(code: any) {
        let res = await fetch('https://teachingserver.onrender.com/cinema/cinemas/' + code)
        let data = await res.json()
        return setCinemaId(data)
    }

    const HandleCinema = (id: any) => {
        return setCurrentCinema(id)
    }

    const HandleActive = (id: any) => {
        return setActive(!id)
    }

    const HandleIndexMovie = (index: any) => {
        return setIndexMovie(index)
    }

    const HandleIndexCinema = (index: any) => {
        return setIndexCinema(index)
    }

    const HandleIndexMovie1 = (index: any) => {
        return setIndexMovie1(index)
    }

    const HandleIndexCinema1 = (index: any) => {
        return setIndexCinema1(index)
    }

    const HandleFilmId = (id: any) => {
        return setFilmId(id)
    }

    const ToTicket = (cid: any, sid: any, cinemaCode: any, screenName: any, showDate: any, showTime: any) => {
        nav('/ticket/' + cid + "/" + sid + '/' + FilmId + '/' + cinemaCode + '/' + screenName + '/' + showDate.replaceAll('/', '%2F') + '/' + showTime)
    }

    return (
        <>
            <HeaderNoTrans />
            <div className="schedule">
                <ul className="nav-tabs">
                    <li onClick={() => setShow(true)} style={{ borderBottom: (Show ? "1px solid salmon" : "none") }}>By Movie</li>
                    <li onClick={() => setShow(false)} style={{ borderBottom: (Show ? "none" : "1px solid salmon") }}>By Cinema</li>
                </ul>
                <div className="tabContent">
                    <div className="showTab" style={{ display: (Show ? "flex" : "none") }}>
                        <div className="byMovie">
                            <div>
                                <h2>Choose Movie</h2>
                            </div>
                            {
                                Film?.movieShowing.map((f: any, i: any) => {
                                    return <div onClick={() => { { HandleFilm(f.id) }; HandleIndexMovie(f.id); HandleFilmId(f.id) }} className={`content ${IndexMovie && f.id === IndexMovie ? 'active' : ''}`} key={f.id}>
                                        <img src={f.imagePortrait} alt="" />
                                        <p>{f.name}</p>
                                        <p>C{f.age}</p>
                                    </div>
                                })
                            }
                        </div>
                        <div className="byCinema">
                            <div>
                                <h2>Choose Cinema</h2>
                            </div>
                            {
                                Schedule?.map((f: any, i: number) => {
                                    return <div onClick={() => { { HandleCinema(f.id) }; HandleIndexCinema(f.id) }} className={`content ${IndexCinema && f.id === IndexCinema ? 'active' : ''}`} key={f.id}>
                                        <p>{f.vistaName}</p>
                                    </div>
                                })
                            }
                        </div>
                        <div className="book">
                            <div className="plan">
                                <h2>Choose Plan</h2>
                            </div>
                            {
                                Schedule?.filter((c: any) => c.id === CurrentCinema).map((s: any, i: number) => {
                                    return <div className="content" key={i}>
                                        {
                                            s.dates.map((d: any, i: number) => {
                                                return <div className="detail" key={i}>
                                                    <p>{d.dayOfWeekLabel},</p>
                                                    <p>{d.showDate}</p>
                                                    <div className="version">
                                                        {
                                                            d.bundles.map((b: any, bi: number) => {
                                                                return <div className="ticketVer" key={bi}>
                                                                    <h5>Ticket {b.version}</h5>
                                                                    <div className="showTime">
                                                                        {
                                                                            b.sessions.map((t: any, ti: number) => {
                                                                                return <button onClick={() => ToTicket(t.cinemaId, t.sessionId, s.code, t.screenName, t.showDate, t.showTime)} key={ti}>{t.showTime}</button>
                                                                            })
                                                                        }
                                                                    </div>
                                                                </div>
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                })
                            }
                        </div>
                    </div>
                    <div className="showTab" style={{ display: (Show ? "none" : "flex") }}>
                        <div className="byCinema">
                            <div>
                                <h2>Choose Cinema</h2>
                            </div>
                            {
                                Cinemas?.map((f: any, i: number) => {
                                    return <div onClick={() => { HandleCinemaId(f.code); HandleCinema(f.id); HandleIndexCinema1(f.id) }} className={`content ${IndexCinema1 && f.id === IndexCinema1 ? 'active' : ''}`} key={f.id}>
                                        <p>{f.vistaName}</p>
                                    </div>
                                })
                            }
                        </div>
                        <div className="byMovie">
                            <div>
                                <h2>Choose Movie</h2>
                            </div>
                            {
                                CinemaId?.map((f: any, i: number) => {
                                    return <div onClick={() => { HandleFilm(f.id); HandleIndexMovie1(f.id); HandleFilmId(f.id) }} className={`content ${IndexMovie1 && f.id === IndexMovie1 ? 'active' : ''}`} key={f.id}>
                                        <img src={f.imagePortrait} alt="" />
                                        <p>{f.name}</p>
                                        <p>C{f.age}</p>
                                    </div>
                                })
                            }
                        </div>
                        <div className="book">
                            <div className="plan">
                                <h2>Choose Plan</h2>
                            </div>
                            {
                                Schedule?.filter((c: any) => c.id === CurrentCinema).map((s: any, i: number) => {
                                    return <div className="content" key={i}>
                                        {
                                            s.dates.map((d: any, i: number) => {
                                                return <div className="detail" key={i}>
                                                    <p>{d.dayOfWeekLabel},</p>
                                                    <p>{d.showDate}</p>
                                                    <div className="version">
                                                        {
                                                            d.bundles.map((b: any, bi: number) => {
                                                                return <div className="ticketVer" key={bi}>
                                                                    <h5>Ticket {b.version}</h5>
                                                                    <div className="showTime">
                                                                        {
                                                                            b.sessions.map((t: any, ti: number) => {
                                                                                return <button onClick={() => ToTicket(t.cinemaId, t.sessionId, s.code, t.screenName, t.showDate, t.showTime)} key={ti}>{t.showTime}</button>
                                                                            })
                                                                        }
                                                                    </div>
                                                                </div>
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            })
                                        }
                                    </div>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
