import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Footer from "../Footer/Footer";
import HeaderNoTrans from "../Header/HeaderNoTrans";
import './Ticket.scss'

export default function Ticket() {

    const navigate = useNavigate();
    const { cinemaId, sessonId, filmId, cinemaCode, screenName, showDate, showTime } = useParams()
    const [Cinema, setCinema] = useState<any>(null);
    const [Show, setShow] = useState<any>(false);
    const [Active, setActive] = useState<any>([]);
    const [ActiveC, setActiveC] = useState<any>([]);
    const [CurrentSelect, setCurrentSelect] = useState<any>(null);
    const [CurrentSelectC, setCurrentSelectC] = useState<any>(null);
    const [Booking, setBooking] = useState<any>(null);
    const [BookingDetail, setBookingDetail] = useState<any>(null);
    const [TicketDetail, setTicketDetail] = useState<any>(null);
    const [ComboDetail, setComboDetail] = useState<any>(null);
    const [SeatPlan, setSeatPlan] = useState<any>(null);
    const [Total, setTotal] = useState<any>(null);
    const [TotalTicket, setTotalTicket] = useState<any>(null);
    const [MaxSeat, setMaxSeat] = useState<number>(0);
    // const [SeatCode, setSeatCode] = useState<number>('');


    useEffect(() => {
        fetch('https://teachingserver.onrender.com/cinema/cinemas/' + cinemaCode)
            .then(res => res.json())
            .then(data => {
                setCinema(data)
            })

        fetch('https://teachingserver.onrender.com/cinema/booking')
            .then(res => res.json())
            .then(data => {
                setBooking(data)
            })

        fetch('https://teachingserver.onrender.com/cinema/booking/detail')
            .then(res => res.json())
            .then(data => {
                let i: number = 0;
                let u: number = 0;
                let newTicketDetail = [];
                let newComboDetail = []
                for (i; i < data.ticket.length; i++) {
                    newTicketDetail.push({ price: 0, quantity: 0, total: 0, ticketTypeCode: '' });
                }
                setTicketDetail(newTicketDetail);
                setBookingDetail(data);
                for (u; u < data.consession[0].concessionItems.length; u++) {
                    newComboDetail.push({ price: 0, quantity: 0, total: 0, description: '' });
                }
                setComboDetail(newComboDetail)
            })
    }, []);

    const currentCinema = Booking?.cinemas.filter((f: any) => f.code === cinemaCode)[0];
    const Movie = Cinema?.filter((f: any) => f.id === filmId)[0];

    const HandleMinus = (i: number, displayPrice: number) => {
        const newTicketDetail = TicketDetail.map((n: any) => n);
        let quantity = Number(newTicketDetail[i].quantity);
        if (quantity > 0) quantity -= 1;
        if (BookingDetail?.ticket[i].packageContent.tickets.length > 0) setMaxSeat(MaxSeat - 2); else if (BookingDetail?.ticket[i]) setMaxSeat(MaxSeat - 1)
        newTicketDetail[i].quantity = quantity;
        newTicketDetail[i].price = displayPrice;
        newTicketDetail[i].total = displayPrice * quantity;
        setTicketDetail(newTicketDetail);
        setTotal(displayPrice * quantity);
        console.log('Max seats: ' + MaxSeat);
    }
    const HandlePlus = (i: number, displayPrice: number) => {
        const newTicketDetail = TicketDetail.map((n: any) => n);
        let quantity = Number(newTicketDetail[i].quantity);
        if (quantity < 99) quantity += 1;
        if (BookingDetail?.ticket[i].packageContent.tickets.length > 0) setMaxSeat(MaxSeat + 2); else if (BookingDetail?.ticket[i]) setMaxSeat(MaxSeat + 1)
        newTicketDetail[i].quantity = quantity;
        newTicketDetail[i].price = displayPrice;
        newTicketDetail[i].total = displayPrice * quantity;
        setTicketDetail(newTicketDetail);
        setTotal(displayPrice * quantity);
        console.log('Max seats: ' + MaxSeat);

    }

    const HandleInput = (i: number, event: any, displayPrice: number) => {
        const newTicketDetail = TicketDetail.map((n: any) => n);
        let value = Number(event.target.value);
        newTicketDetail[i].quantity = value;
        newTicketDetail[i].price = displayPrice;
        newTicketDetail[i].total = displayPrice * value;
        setTicketDetail(newTicketDetail);
        setTotal(displayPrice * value);
    }

    const HandleMinusC = (i: number, displayPrice: number, description: any) => {
        const newComboDetail = ComboDetail.map((n: any) => n);
        let quantity = Number(newComboDetail[i].quantity);
        if (quantity > 0) quantity -= 1;
        if (quantity < 0) newComboDetail[i].description = '';
        newComboDetail[i].quantity = quantity;
        newComboDetail[i].price = displayPrice;
        newComboDetail[i].total = displayPrice * quantity;
        setComboDetail(newComboDetail);
        setTotal(displayPrice * quantity);
    }
    const HandlePlusC = (i: number, displayPrice: number, description: any) => {
        const newComboDetail = ComboDetail.map((n: any) => n);
        let quantity = Number(newComboDetail[i].quantity);
        if (quantity < 99) quantity += 1;
        if (quantity > 0) newComboDetail[i].description = description
        newComboDetail[i].quantity = quantity;
        newComboDetail[i].price = displayPrice;
        newComboDetail[i].total = displayPrice * quantity;
        setComboDetail(newComboDetail);
        setTotal(displayPrice * quantity);
    }

    const HandleInputC = (i: number, event: any, displayPrice: number, description: any) => {
        const newComboDetail = ComboDetail.map((n: any) => n);
        let value = Number(event.target.value);
        if (value < 0) newComboDetail[i].description = '';
        if (value > 0) newComboDetail[i].description = description
        newComboDetail[i].quantity = value;
        newComboDetail[i].price = displayPrice;
        newComboDetail[i].total = displayPrice * value;
        setComboDetail(newComboDetail);
        setTotal(displayPrice * value);
    }

    let TicketPrice: any = TicketDetail?.reduce((total: any, current: any) => total + current.total, 0);
    let ComboPrice: any = ComboDetail?.reduce((total: any, current: any) => total + current.total, 0);
    let TicketAmount: any = TicketDetail?.reduce((total: any, current: any) => total + current.quantity, 0)

    const HandleTotal = (event: any) => {
        setTotal(event.target.value)
    }
    const handlePayment = () => {
        navigate('/creditcard', { state: { amount: Total } });
    }

    const HandleClick = (rowIndex: any, columnIndex: any, areaNumber: any, index: any, physicalName: any) => {
        const newActive: any = Active.map((n: any) => n);
        let newInActive = newActive.findIndex((e: any, i: number) => e.rowIndex === rowIndex && e.columnIndex === columnIndex && e.areaNumber === areaNumber)
        if (newInActive != -1) {
            newActive.splice(newInActive, 1);
        }
        else newActive.push({ rowIndex: rowIndex, columnIndex: columnIndex, areaNumber: areaNumber, index: index, physicalName: physicalName })
        setActive(newActive)
    }

    const HandleCurrentSelectCouple = (sic: any) => {
        setCurrentSelectC(sic)
    }

    const HandleCurrentSelect = (si: any) => {
        setCurrentSelect(si)
    }

    const HandleComplete = () => {
        let ticketSeats = Active.map((seat: any) => { return seat.physicalName + seat.index.toString() }).join(', ');
        // setSeatCode(ticketSeats)
        let showdate = showDate?.split('/');
        let timestamp = '';
        if (showdate != undefined) {
            timestamp = new Date(showdate[2] + '-' + showdate[1] + '-' + showdate[0] + 'T' + showTime?.toString()).toISOString();
        }

        let TicketInfo = {
            "BankId": 0,
            "CardNumber": "5555 4444 3333 1111",
            "CardName": "John Smith",
            "ExpireDate": "10/20",
            "CVV": "737",
            "Price": Total,
            "ShowCode": cinemaId + '-' + sessonId,
            "Email": "user@example.com",
            "CinemaName": currentCinema?.vistaName,
            "TheaterName": screenName,
            "FilmName": Movie?.name,
            "ImageLandscape": Movie?.imageLandscape,
            "ImagePortrait": Movie?.imagePortrait,
            "Combo": "string",
            "SeatCode": ticketSeats,
            "ShowTime": timestamp
        }

        fetch(`https://vietcpq.name.vn/U2FsdGVkX19udsrsAUnUBsRg8K4HmweHVb4TTgSilDI=/cinema/Ticket`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(TicketInfo)
            }).then(() => handlePayment())

            ;
    }

    return (
        <>

            <HeaderNoTrans />
            <div className="ticket">
                <div className="leftTab" style={{ display: (Show ? "none" : "block") }}>
                    <h2>Choose Ticket/Food</h2>
                    <div className="ticketBox">
                        <table className="w-100">
                            <thead className="ta-c">
                                <tr>
                                    <th className="w-40">Type</th>
                                    <th className="w-30">Quantity</th>
                                    <th className="w-15">Price(VND)</th>
                                    <th className="w-15">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    BookingDetail?.ticket.map((b: any, i: number) => {
                                        return (
                                            <tr key={i}>
                                                <td>{b.name} <br /> {b.description}</td>
                                                <td className="ta-c"><button onClick={() => HandleMinus(i, b.displayPrice)}>-</button><input className="w-30 ta-c" type="number" name="" id="" value={TicketDetail.length > 0 ? TicketDetail[i].quantity : 0} onChange={e => HandleInput(i, e, b.displayPrice)} /><button onClick={() => HandlePlus(i, b.displayPrice)}>+</button></td>
                                                <td className="ta-c">{b.displayPrice}</td>
                                                <td className="ta-c">{TicketDetail.length > 0 ? b.displayPrice * TicketDetail[i].quantity : 0} </td>
                                            </tr>
                                        )
                                    })
                                }
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td className="ta-c">{TicketDetail?.reduce((total: any, current: any) => total + current.total, 0)}</td>
                                </tr>
                            </tbody>
                            <thead className="ta-c">
                                <tr>
                                    <th className="w-40">Combo</th>
                                    <th className="w-30">Quantity</th>
                                    <th className="w-15">Price(VND)</th>
                                    <th className="w-15">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    BookingDetail?.consession[0].concessionItems.map((b: any, i: number) => {
                                        return (
                                            <tr key={i}>
                                                <td><div className="combo"><img src={b.imageUrl} alt="" /><div><h3>{b.description}</h3> <br /> <h4>{b.extendedDescription}</h4></div></div></td>
                                                <td className="ta-c"><button onClick={() => HandleMinusC(i, b.displayPrice, b.description)}>-</button><input className="w-30 ta-c" type="number" name="" id="" value={ComboDetail.length > 0 ? ComboDetail[i].quantity : 0} onChange={e => HandleInputC(i, e, b.displayPrice, b.description)} /><button onClick={() => HandlePlusC(i, b.displayPrice, b.description)}>+</button></td>
                                                <td className="ta-c">{b.displayPrice}</td>
                                                <td className="ta-c">{ComboDetail.length > 0 ? b.displayPrice * ComboDetail[i].quantity : 0} </td>
                                            </tr>
                                        )
                                    })
                                }
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td className="ta-c">{ComboDetail?.reduce((total: any, current: any) => total + current.total, 0)}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div className="leftTab" style={{ display: (Show ? "block" : "none") }}>
                    <div className="seatPlan">
                        <div className="seatTable">
                            {
                                BookingDetail?.seatPlan.seatLayoutData.areas[1].rows.map((p: any, pi: number) => {
                                    return <ul className="ulParent" key={pi}>
                                        <li className="pNameF">{p.physicalName}</li>
                                        {
                                            p.seats.map((s: any, sic: number) => {
                                                return <li onClick={() => { HandleClick(s.position.rowIndex, s.position.columnIndex, s.position.areaNumber, sic, p.physicalName); HandleCurrentSelect(sic) }} className={`middleLi ${Active.find((e: any) => e.rowIndex === s.position.rowIndex && e.columnIndex === s.position.columnIndex && e.areaNumber === s.position.areaNumber) != undefined ? 'active' : ''}`} key={sic} >
                                                    {sic}
                                                </li>
                                            })
                                        }
                                        <li className="pNameL">{p.physicalName}</li>
                                    </ul>
                                })
                            }
                            {
                                BookingDetail?.seatPlan.seatLayoutData.areas[0].rows.map((p: any, pi: number) => {
                                    return <ul className="ulParent" key={pi}>
                                        <li className="pNameF">{p.physicalName}</li>
                                        {
                                            p.seats.map((s: any, si: number) => {
                                                return <li onClick={() => { HandleClick(s.position.rowIndex, s.position.columnIndex, s.position.areaNumber, si, p.physicalName); HandleCurrentSelect(si) }} className={`middleLi ${Active.find((e: any) => e.rowIndex === s.position.rowIndex && e.columnIndex === s.position.columnIndex && e.areaNumber === s.position.areaNumber) != undefined ? 'active' : ''}`} key={si} >
                                                    {si}
                                                </li>
                                            })
                                        }
                                        <li className="pNameL">{p.physicalName}</li>
                                    </ul>
                                })
                            }
                        </div>
                        <div className="screen">
                            <h3>Screen</h3>
                        </div>
                        <div className="screenBot">
                            <p><span className="green"></span> Current Select</p>
                            <p><span className="red"></span> Booked</p>
                            <p><span className="gray"></span> Available</p>
                            <p><span className="cyan"></span> Unavailable</p>
                        </div>
                    </div>
                </div>
                <div className="rightTab">
                    <div className="topRight">
                        <img src={Movie?.imageLandscape} alt="" />
                        <h2>{Movie?.name}</h2>
                        <h2 className="blur">{Movie?.subName}</h2>
                        <span>C{Movie?.age}</span><p className="warning">(*) Only for audiences from 13</p>
                        <h3>{currentCinema?.vistaName} | {screenName}</h3>
                        <h3>Showtime: {showTime} | {showDate}</h3>
                        <h3>Combo: {ComboDetail?.filter((combo: any) => { return combo.quantity > 0 && combo.description }).map((combo: any) => { return combo.description }).join(', ')}</h3>
                        {/* <h3>Seat: {SeatCode}</h3> */}
                        <h2 className="sum">Total: <input className="total" onChange={(e) => HandleTotal(e)} type="number" value={TicketPrice + ComboPrice > 0 ? TicketPrice + ComboPrice : 0} name="" id="" /><span className="vnd">VND</span></h2>
                        <ul>
                            <li>
                                <button className="continue" style={{ display: (Show ? "none" : "block") }} onClick={() => { setShow(true); }}>CONTINUE {'->'}</button>
                                <button style={{ display: (Show ? "block" : "none") }} onClick={() => setShow(false)}>{`<-`} GO BACK </button>
                                <button onClick={HandleComplete} style={{ display: (Show ? "block" : "none") }}>COMPLETE {'->'}</button>
                                {/* <button style={{ display: (Show ? "block" : "none") }} onClick={handlePayment}>COMPLETE {'->'}</button> */}
                            </li>
                        </ul>
                    </div>
                    <div className="botRight">

                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

