import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import HeaderNoTrans from "../Header/HeaderNoTrans";
import './MovieShowing.scss'

export default function MovieShowing() {
    const [lsfilm, setLsFilm] = useState<any>([])
    const nav = useNavigate()

    useEffect(() => {
        fetch('https://teachingserver.onrender.com/cinema/nowAndSoon')
            .then(res => res.json())
            .then(data => {
                setLsFilm(data)
            })
    }, []);

    const ToComingSoon = () => {
        nav('/moviecomingsoon')
    }

    const ToMovieDetail = (id: any) => {
        nav('/moviedetail/' + id)
    }

    return (
        <>
            <HeaderNoTrans />
            <div className="movieShowing">
                <div className="tag">
                    <h3 className="activePage">Movie Showing</h3>
                    <h3 onClick={ToComingSoon}>Movie Coming Soon</h3>
                </div>
                {
                    lsfilm.movieShowing?.map((s: any, i: number) => {
                        return <div key={i} className='card'>
                            <div className="hover" onClick={() => ToMovieDetail(s.id)}>
                                <img className="image" src={`${s.imagePortrait}`} alt="" />
                                <div className="middle">
                                    <div onClick={() => ToMovieDetail(s.id)} className="btn">Book</div>
                                </div>
                            </div>
                            <p>{s.name}</p>
                        </div>
                    })
                }
            </div>
            <Footer />
        </>
    )
}
