import { type } from "@testing-library/user-event/dist/type"
import React, { useEffect, useState } from "react"
import { useNavigate } from 'react-router-dom'
import Footer from "../Footer/Footer"
import Header from "../Header/Header"
import './Home.scss'
import ImageSlider from "./ImageSlider/ImageSlider"

export default function Home() {

    const [lsFilm, setlsFilm] = useState<any>([]);
    const nav = useNavigate();

    useEffect(() => {
        fetch(`https://teachingserver.onrender.com/cinema/nowAndSoon`)
            .then(res => res.json())
            .then(data => {
                setlsFilm(data)
            })
    }, [])

    const slides = [
        { url: 'https://staticg.sportskeeda.com/editor/2023/02/388f5-16763770759494-1920.jpg', title: 'Ant-Man And The Wasp: Quantumania', id: 'df7885d7-a8e2-4646-bad2-1f19e6905961' },
        { url: 'https://images2.thanhnien.vn/Uploaded/nhuvnq/2023_01_13/01-teaser-nbn-3189.jpg', title: 'Nhà Bà Nữ', id: '38e37ef8-592e-4492-aa20-99f2b306e30d' },
        { url: 'https://upload.wikimedia.org/wikipedia/vi/7/7c/Newone_-_Ch%E1%BB%8B_ch%E1%BB%8B_em_em_2_poster.jpg', title: 'Chị Chị Em Em 2', id: 'c2d0d359-8fae-4d01-a433-d81c8483473d' },
        { url: 'https://www.cgv.vn/media/catalog/product/cache/1/image/1800x/71252117777b696995f01934522c402d/1/9/1920x1080-vong-nhi.jpg', title: 'Vong Nhi', id: '212ebb08-d523-4907-9537-32dd6c6f1e35' },
        { url: 'https://media-cdn-v2.laodong.vn/storage/newsportal/2023/1/13/1137924/Titanic-Wallpaper.jpeg', title: 'TITANIC', id: '43665dc4-7960-4adf-8233-f6e6b203e8f6' },
        { url: 'https://i0.wp.com/anitrendz.net/news/wp-content/uploads/2022/08/swordartonlineprogressivescherzoofdeepnight_keyvisual2-1-scaled-e1661164219704.jpg?fit=1811%2C1017&amp;ssl=1', title: 'Sword Art Online The Movie: Progressive Scherzo Of Deep Night', id: '2b4c504c-de2e-48b4-ac62-73cd1eddac13' },
        { url: 'https://cdn.galaxycine.vn/media/2023/2/10/913x391_1676000558245.jpg', title: 'Chuyến Đi Nhớ Đời: Tiểu Đội Gấu Bay', id: '72ef6207-568b-4911-b1da-5bf47be6d642' }
    ]


    const ShowFilm = (id: number): void => {
        nav('/Films/' + id)
    }

    const containerStyles = {
        width: '100%',
        height: '100vh',
        margin: "0 auto"
    }

    const ToMovieDetail = (id: any) => {
        nav('/moviedetail/' + id)
    }

    const ToShowing = () => {
        nav('/movieshowing')
    }

    const ToComingSoon = () => {
        nav('/moviecomingsoon/')
    }

    const [Show, setShow] = useState(true)

    return (
        <>
            <div style={containerStyles}>
                {/* <Header /> */}
                <ImageSlider slides={slides} />
                <div className="movies">
                    <ul className="nav-tabs">
                        <li onClick={() => setShow(true)} style={{ borderBottom: (Show ? "1px solid salmon" : "none") }}>Showing</li>
                        <li onClick={() => setShow(false)} style={{ borderBottom: (Show ? "none" : "1px solid salmon") }}>Coming Soon</li>
                    </ul>
                    <div className="tab-content">
                        <div id="showing" className="flex flex-wrap show-tab" style={{ display: (Show ? "flex" : "none") }}>
                            {
                                lsFilm.movieShowing?.map((c: any, i: number) => {
                                    return i < 6 && <div key={i} className='card'>
                                        <div className="hover" onClick={() => ToMovieDetail(c.id)}>
                                            <img className="image" src={`${c.imagePortrait}`} alt="" />
                                            <div className="middle">
                                                <div onClick={() => ToMovieDetail(c.id)} className="btn">Book</div>
                                            </div>
                                        </div>
                                        <p>{c.name}</p>
                                    </div>
                                })
                            }
                            <div className="more-btn">
                                <button onClick={ToShowing}>More {'-->'}</button>
                            </div>
                        </div>
                        <div id="comingsoon" className="flex flex-wrap show-tab" style={{ display: (Show ? "none" : "flex") }}>
                            {
                                lsFilm.movieCommingSoon?.map((c: any, i: number) => {
                                    return i < 6 && <div key={i} className='card'>
                                        <div className="hover" onClick={() => ToMovieDetail(c.id)}>
                                            <img className="image" src={`${c.imagePortrait}`} alt="" />
                                            <div className="middle">
                                                <div onClick={() => ToMovieDetail(c.id)} className="btn">Book</div>
                                            </div>
                                        </div>
                                        <p>{c.name}</p>
                                    </div>
                                })
                            }
                            <div className="more-btn">
                                <button onClick={ToComingSoon}>More {'-->'}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </>
    )
}
