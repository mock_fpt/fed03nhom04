import React, { useState } from "react";
import Header from "../../Header/Header";
import '../ImageSlider/ImageSlider.scss';
import { useNavigate } from "react-router-dom";

export default function ImageSlider({ slides }: any) {
    const [currentIndex, setcurrentIndex] = useState(0);
    const nav = useNavigate();
    const slideStyles = {
        width: '100%',
        height: '100%',
        // borderRadius: '10px',
        backgroundPosition: 'center',
        backgroundRepeat: "no-repeat",
        backgroundSize: 'cover',
        backgroundImage: `url(${slides[currentIndex].url})`,
        transition: `0.6s ease`,
        display: "flex",
        justifyContent: "center",
        // alignItems: "end",
    }

    const goToPrevious = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
        setcurrentIndex(newIndex)
    }

    const goToNext = () => {
        const isLastSlide = currentIndex === slides.length - 1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setcurrentIndex(newIndex)
    }

    const gotoSlide = (slideIndex: number) => {
        setcurrentIndex(slideIndex)
    }

    const ToMovieDetail = (id: any) => {
        nav('/moviedetail/' + id)
    }

    return (
        <div className='sliderStyles' >
            <div className="leftArrowStyles" onClick={goToPrevious}>←</div>
            <div className="rightArrowStyles" onClick={goToNext}>→</div>
            <button className="middle" onClick={() => ToMovieDetail(slides[currentIndex].id)}>Detail</button>
            <div style={slideStyles} className="fade">
                <Header />
                <div className="bullets">
                    {slides.map((slide: any, slideIndex: number) => (
                        <div key={slideIndex} onClick={() => gotoSlide(slideIndex)}>•</div>
                    ))}
                </div>
            </div>
        </div>
    )
}
