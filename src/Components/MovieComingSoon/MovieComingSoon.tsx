import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import HeaderNoTrans from "../Header/HeaderNoTrans";
import './MovieComingSoon.scss'

export default function MovieComingSoon() {
    const [lsfilm, setLsFilm] = useState<any>([])
    const nav = useNavigate()

    useEffect(() => {
        fetch('https://teachingserver.onrender.com/cinema/nowAndSoon')
            .then(res => res.json())
            .then(data => {
                setLsFilm(data)
            })
    }, []);

    const ToShowing = () => {
        nav('/movieshowing')
    }

    const ToMovieDetail = (id: any) => {
        nav('/moviedetail/' + id)
    }

    return (
        <>
            <HeaderNoTrans />
            <div className="movieComingSoon">
                <div className="tag">
                    <h3 onClick={ToShowing}>Movie Showing</h3>
                    <h3 className="activePage">Movie Coming Soon</h3>
                </div>
                {
                    lsfilm.movieCommingSoon?.map((c: any, i: number) => {
                        return <div key={i} className='card'>
                            <div className="hover" onClick={() => ToMovieDetail(c.id)}>
                                <img className="image" src={`${c.imagePortrait}`} alt="" />
                                <div className="middle">
                                    <div onClick={() => ToMovieDetail(c.id)} className="btn">Book</div>
                                </div>
                            </div>
                            <p>{c.name}</p>
                        </div>
                    })
                }
            </div>
            <Footer />
        </>
    )
}
