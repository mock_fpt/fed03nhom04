import React, { useState } from 'react'
import Card from "react-credit-card-component";
import "react-credit-card-component/dist/styles-compiled.css";
import { useLocation, useNavigate } from 'react-router-dom';
import { actionCheckCard, actionUpdateBalance } from '../../Redux/reducers/CreditCard/credit.action';
import { BankCardType } from '../../Redux/reducers/CreditCard/credit.actionType';
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks';
import { RootState } from '../../Redux/stores/Store';
import './CreditCard.scss'

type Props = {}

const CreditCard = (props: Props) => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const location = useLocation();
    let payment = location.state?.amount;
    const { creditCard, checkCard } = useAppSelector((state: RootState) => state.rdcCreditCard);
    // console.log(creditCard);
    // console.log(payment);


    const [state, setState] = useState({
        number: '',
        name: '',
        expiry: '',
        cvc: '',
        issuer: '',
        focused: '',
        formData: null
    })

    const [bankCard, setBankCard] = useState<BankCardType>(
        {
            "BankId": 1,
            "CardNumber": "",
            "CardName": "",
            "ExpireDate": "",
            "CVV": ""
        }
    )
    const [isShow, setIsShow] = useState<boolean>(false)
    const handleSelectBank = (id: number) => {
        setBankCard({ ...bankCard, BankId: id })
        setIsShow(true);
    }

    const handleInputFocus = (e: React.FormEvent<HTMLInputElement>) => {
        setState({ ...state, focused: e.currentTarget.name });
    };

    const handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        const { name, value } = e.currentTarget;

        setState({ ...state, [name]: value });
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        let cardInfo = {
            "BankId": bankCard.BankId,
            "CardNumber": state.number,
            "CardName": state.name,
            "ExpireDate": state.expiry,
            "CVV": state.cvc
        }
        dispatch(actionCheckCard(cardInfo));

        if (checkCard) {
            let newBalance = creditCard.Balance - payment;
            if (newBalance < 0) {
                alert("You don't have enough money in your account")
                return;
            }
            let updatedBalanceCardInfo = { ...creditCard, "Balance": newBalance }

            dispatch(actionUpdateBalance(updatedBalanceCardInfo))
            setTimeout(function () {
                navigate('/profile')
            }, 100);
        }

        // e.currentTarget.reset();
    }


    return (
        <div id="PaymentForm" className='creditCard'>
            {!isShow && <>
                <h3>Please select your payment method:</h3>
                <div className='cardList'>
                    <div className='card1' onClick={() => handleSelectBank(1)}>
                        <Card
                            name="John Smith"
                            number="5555 4444 3333 1111"
                            expiry="10/20"
                            cvc="737"
                        />
                    </div>
                    <div className='card2' onClick={() => handleSelectBank(2)}>
                        <Card
                            name="John Smith"
                            number="5066 9911 1111 1118"
                            expiry="10/20"
                            cvc="737"
                        />
                    </div>
                </div>
            </>}

            {isShow && <div><Card
                cvc={state.cvc}
                expiry={state.expiry}
                focused={state.focused}
                name={state.name}
                number={state.number}
            />
                <form onSubmit={(e) => handleSubmit(e)}>
                    <div className='cardItem'>
                        <input
                            type="tel"
                            name="number"
                            placeholder="Card Number"
                            pattern='[\d| ]{16,22}'
                            maxLength={19}
                            // required
                            onChange={handleInputChange}
                            onFocus={handleInputFocus}
                        />
                    </div>
                    <div className='cardItem'>
                        <input
                            type="tel"
                            name="name"
                            pattern='[a-z A-Z-]+'
                            // required
                            placeholder="Card Name"
                            onChange={handleInputChange}
                            onFocus={handleInputFocus}
                        />
                    </div>
                    <div className='cardItem'>
                        <input
                            type="string"
                            name="expiry"
                            placeholder="Expiration Date"
                            pattern='\d\d\d\d'
                            // required
                            onChange={handleInputChange}
                            onFocus={handleInputFocus}

                        />
                        <input
                            type="tel"
                            name="cvc"
                            placeholder="CVC"
                            pattern='\d{3}'
                            // required
                            onChange={handleInputChange}
                            onFocus={handleInputFocus}
                        />
                    </div>
                    <button className='cardBtn'>Submit</button>
                </form>
            </div>}
        </div>
    )
}

export default CreditCard;

function checkBankCard(card1: { BankId: number; CardNumber: string; CardName: string; ExpireDate: string; CVV: string; }) {
    throw new Error('Function not implemented.');
}
