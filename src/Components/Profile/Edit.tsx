import React, { useEffect, useState } from 'react'
import { actionUpdateUser, getAll } from '../../Redux/reducers/User/user.actions'
import { UserItemType, UserUpdateType } from '../../Redux/reducers/User/user.actionTypes'
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks'
import { RootState } from '../../Redux/stores/Store'

type Props = {}
type ErrorMsgType = {
  Email: string,
  Name: string,
  Password: string,
  repeatPassword: string
}

const Edit = (props: Props) => {
  const { user } = useAppSelector((state: RootState) => state.rdcUser);
  // console.log(user);
  const dispatch = useAppDispatch();

  const [userEdit, setUserEdit] = useState<UserUpdateType & ErrorMsgType>({
    Email: '',
    Name: '',
    Password: '',
    repeatPassword: ''
  })

  useEffect(() => {
    getAll().then((result) => {
      // console.log(result);

      let userFind = result.find((item: UserItemType) => item.Email == user?.Email)
      setUserEdit({ ...userEdit, Email: userFind.Email, Name: userFind.Name })
    }).catch(console.error.bind(console));
  }, []);




  const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    for (let item of checkList) {
      let text = userEdit[item.id as keyof UserUpdateType];
      errorMsg[item.id as keyof ErrorMsgType] = '';
      if (text == '') {
        setErrorMsg({ ...errorMsg, [item.id]: item.messEmpty })
        return;
      }
      if (item.regex.test(text) == false) {
        setErrorMsg({ ...errorMsg, [item.id]: item.messError })
        return;
      }
      if (item.id == 'repeatPassword' && userEdit['Password'] != userEdit['repeatPassword']) {
        setErrorMsg({ ...errorMsg, [item.id]: item.messError })
        return;
      }
    }
    let userInfo: UserUpdateType = {
      Email: userEdit.Email,
      Name: userEdit.Name,
      Password: userEdit.Password
    }
    // console.log(userInfo);

    actionUpdateUser(userInfo)

  }
  const handleChangeInput = (e: React.FormEvent<HTMLInputElement>): void => {
    setUserEdit({ ...userEdit, [e.currentTarget.name]: e.currentTarget.value })
  }


  const [errorMsg, setErrorMsg] = useState<ErrorMsgType>({
    Email: '',
    Name: '',
    Password: '',
    repeatPassword: ''
  })

  const checkList = [
    {
      id: 'Name',
      idError: 'Name',
      regex: /^.+$/,
      messEmpty: 'The Last Name should not be blank.',
      messError: ''
    },
    {
      id: 'Email',
      idError: 'Email',
      regex: /^\w+@\w+(\.\w+)+$/,
      messEmpty: 'The Email should not be blank.',
      messError: 'Please enter an e-mail address.'
    },
    {
      id: 'Password',
      idError: 'Password',
      regex: /^.+$/,
      messEmpty: 'Please enter a password.',
      messError: ''
    },
    {
      id: 'repeatPassword',
      idError: 'repeatPassword',
      regex: /^.+$/,
      messEmpty: 'Please repeat the password.',
      messError: 'The passwords did not match.'
    },

  ]

  return (
    <div className='editProfile'>
      <form onSubmit={(e) => handleSubmit(e)}>
        <div className='editItem'>
          <div className='editInput'>
            <p>Username*</p>
            <input name='Name' value={userEdit.Name} onChange={(e) => handleChangeInput(e)} required />
            {errorMsg.Name && <p className='errorMsg'>{errorMsg.Name}</p>}
          </div>
          <div className='editInput'>
            <p>Email*</p>
            <input name='Email' value={userEdit.Email} onChange={(e) => handleChangeInput(e)} required />
            {errorMsg.Email && <p className='errorMsg'>{errorMsg.Email}</p>}
          </div>
        </div>

        <div className='editItem'>
          <div className='editInput'>
            <p>Password *</p>
            <input name='Password' onChange={(e) => handleChangeInput(e)} required />
            {errorMsg.Password && <p className='errorMsg'>{errorMsg.Password}</p>}
          </div>
          <div className='editInput'>
            <p>Repeat Password *</p>
            <input name='repeatPassword' onChange={(e) => handleChangeInput(e)} required />
            {errorMsg.repeatPassword && <p className='errorMsg'>{errorMsg.repeatPassword}</p>}
          </div>
        </div>
        <button className='editBtn'>Save profile</button>
      </form>
    </div>
  )
}

export default Edit