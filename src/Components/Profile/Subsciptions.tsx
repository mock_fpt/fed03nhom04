import { Link, useNavigate } from 'react-router-dom'
import { actionDeleteUser, deleteCookie, getCookie } from '../../Redux/reducers/User/user.actions'
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks'
import { RootState } from '../../Redux/stores/Store'
import './Profile.scss'

type Props = {}

const Subsciptions = (props: Props) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { payment } = useAppSelector((state: RootState) => state.rdcSubscription);
  const { user } = useAppSelector((state: RootState) => state.rdcUser);

  const handlePayment = (type: string) => {
    switch (type) {
      case "Premium":
        return "Premium $39 / 3 Months with a 5 days free trial"
      case "Basic":
        return "Basic $19 / 1 Month"
      default:
        return "Free"
    }
  }
  const handleDelete = () => {
    window.confirm("Are you sure you wish to delete this account?") && dispatch(actionDeleteUser(user?.Email || ''))
    navigate('/');
    deleteCookie('movie');
  }
  return (
    <div className='subscriptions'>
      <table>
        <tbody>
          <tr>
            <td>Subscription Plan</td>
            <td>{handlePayment(payment.type)}</td>
          </tr>
          <tr >
            <td>Status</td>
            <td>
              Active
            </td>
          </tr>
          <tr >
            <td>Start Date</td>
            <td>February 6, 2023</td>
          </tr>
          <tr >
            <td>Expiration Date</td>
            <td>Unlimited</td>
          </tr>
          <tr >
            <td>Actions</td>
            <td>
              <Link to={'/profile/upgrade'} className="subscriptionBtn">Change</Link>
              <Link to={'/home'} className="subscriptionBtn">Cancle</Link>
              <span onClick={handleDelete} className="subscriptionBtn">Abandon</span>

            </td>
          </tr>
        </tbody>
      </table>
    </div >
  )
}

export default Subsciptions;