import React from 'react'
import { useAppSelector } from '../../Redux/stores/hooks';
import { RootState } from '../../Redux/stores/Store';

type Props = {}

const Payments = (props: Props) => {
  const { payment } = useAppSelector((state: RootState) => state.rdcSubscription);
  console.log(payment);
  const handlePayment = (type: string) => {
    switch (type) {
      case "Premium":
        return "Premium $39 / 3 Months with a 5 days free trial"
      case "Basic":
        return "Basic $19 / 1 Month"
      default:
        return "No payments found"
    }
  }
  return (
    <div className='payment'>
      <div className='paymentInfo'>
        <p> {handlePayment(payment.type)}</p>
      </div>
    </div>
  )
}

export default Payments