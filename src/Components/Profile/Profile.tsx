import { useEffect, useState } from 'react';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import { actionLogout, getAll } from '../../Redux/reducers/User/user.actions';
import { UserItemType } from '../../Redux/reducers/User/user.actionTypes';
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks';
import { RootState } from '../../Redux/stores/Store';
import Edit from './Edit';
import Payments from './Payments'
import './Profile.scss'
import Subsciptions from './Subsciptions';

type Props = {}



const Profile = (props: Props) => {
    const dispatch = useAppDispatch();
    const { user } = useAppSelector((state: RootState) => state.rdcUser);
    const { payment } = useAppSelector((state: RootState) => state.rdcSubscription);
    console.log(payment);
    
    const [userList, setUserList] = useState<UserItemType[]>([])

    useEffect(() => {
        getAll().then((result) => {
            setUserList(result)
        }).catch(console.error.bind(console));
    }, []);

    // console.log(userList);

    let userEdit = userList.find(item => item.Email == user?.Email)



    const [currentTab, setCurrentTab] = useState('0');


    const tabs = [
        {
            id: '0',
            tabTitle: 'subscriptions',
            content: <Subsciptions />
        },
        {
            id: '1',
            tabTitle: 'edit profile',
            content: <Edit />
        },
        {
            id: '2',
            tabTitle: ' payments',
            content: <Payments />
        },
        {
            id: '3',
            tabTitle: 'Logout',
            content: ''
        }
    ];



    const handleTabClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        let id = e.currentTarget.id;
        if (id == '3') {
            dispatch(actionLogout())
        }
        else setCurrentTab(e.currentTarget.id);
    }


    return (
        <>
            <div className='profile'>
                <div className='profileTitle'>
                    <h2 className='fs-50 color-white pb-30'>Account</h2>
                    <p className='profileNavlink'><a href='/home'>Home</a>  &gt;  Account</p>
                </div>
                <div className='profileContent flex'>
                    <div className='userCard w-30'>
                        <img src='https://secure.gravatar.com/avatar/928c7369d49bd54ecba6827c9f1c22d8?s=96&d=mm&r=g' />
                        {/* <p><i className="fa fa-edit" aria-hidden="true"></i> Edit</p> */}
                        {userEdit && <h4 className='mt-5'>{userEdit.Name}</h4>}
                    </div>
                    <div className='userContent w-70'>
                        {tabs.map((tab, i) =>
                            <button key={i} id={tab.id} disabled={currentTab == tab.id} className='profileTab' onClick={(e) => handleTabClick(e)}>{tab.tabTitle}</button>
                        )}
                        <div>
                            {tabs.map((tab, i) =>
                                <div key={i}>
                                    {currentTab == tab.id && <div>{tab.content}</div>}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>


        </>
    )
}

export default Profile;