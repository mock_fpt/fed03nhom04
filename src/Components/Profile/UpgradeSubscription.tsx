import React, { useEffect, useState } from 'react'
import { Navigate, useNavigate } from 'react-router-dom';
import { actionSetSubscription } from '../../Redux/reducers/Payment/payment.action';
import { cookieCreator, getAll } from '../../Redux/reducers/User/user.actions';
import { UserItemType } from '../../Redux/reducers/User/user.actionTypes';
import { useAppDispatch, useAppSelector } from '../../Redux/stores/hooks';
import { RootState } from '../../Redux/stores/Store';
import './Profile.scss'


type Props = {}



const UpgradeSubscription = (props: Props) => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const { user } = useAppSelector((state: RootState) => state.rdcUser);

    const [userList, setUserList] = useState<UserItemType[]>([])

    useEffect(() => {
        getAll().then((result) => {
            setUserList(result)
        }).catch(console.error.bind(console));
    }, []);


    let userEdit = userList.find(item => item.Email == user?.Email)

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();


        let subscriptionPlan = {
            type: checkState1 == 0 ? 'Premium' : 'Basic',
            method: checkState2 == 0 ? 'Offline' : 'Credit',
            amount: checkState2 == 0 ? 0 : checkState1 == 0 ? 39 : 19
        }
        console.log(subscriptionPlan);

        actionSetSubscription(subscriptionPlan);
        dispatch(actionSetSubscription(subscriptionPlan))
        cookieCreator('subscriptionPlan', JSON.stringify(subscriptionPlan), 1440)

        subscriptionPlan.amount && navigate('/creditcard', { state: { amount: subscriptionPlan.amount } });
    
    }

    const option1 = [
        { id: 0, label: "Premium -$39 / 3 Months with a 5 days free trial" },
        { id: 1, label: "Basic -$19 / 1 Month" },
    ];
    const option2 = [
        { id: 0, label: "Manual / Offline" },
        { id: 1, label: "Credit Card" },
    ];

    const [checkState1, setCheckState1] = useState<number>(0)
    const [checkState2, setCheckState2] = useState<number>(0)


    return (
        <div className='profile'>
            <div className='profileTitle'>
                <h2 className='fs-50 color-white pb-30'>Account</h2>
                <p className='profileNavlink'><a href='/home'>Home</a>  &gt;  Account</p>
            </div>
            <div className='profileContent flex'>
                <div className='userCard w-30'>
                    <img src='https://secure.gravatar.com/avatar/928c7369d49bd54ecba6827c9f1c22d8?s=96&d=mm&r=g' />
                    {userEdit && <h4 className='mt-5'>{userEdit.Name}</h4>}
                </div>
                <div className='userContent w-70'>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className='upgradeContent'>
                            <h3>Upgrade <b>Free</b> to:</h3>
                            {
                                option1.map((n, i) => {
                                    return <div key={i} className='registerPlanItem' onClick={() => setCheckState1(i)}>
                                        <input type="checkbox" checked={checkState1 == n.id} onChange={() => setCheckState1(i)} />
                                        <label>{n.label}</label>
                                    </div>
                                })
                            }
                            <h3>Select a Payment Method</h3>
                            {
                                option2.map((n, i) => {
                                    return <div key={i} className='registerPlanItem' onClick={() => setCheckState2(i)}>
                                        <input type="checkbox" checked={checkState2 == n.id} onChange={() => setCheckState2(i)} />
                                        <label>{n.label}</label>
                                    </div>
                                })
                            }
                            <h3>Discount Code:</h3>
                            <div className='registerPlanItem'>
                                <input className='discountInput' placeholder='Enter discount' />
                                <button className='discountBtn'>Apply</button>
                            </div>
                            <button className='registerBtn' >Upgrade subscription</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default UpgradeSubscription;