import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import './Header.scss'

export default function Header(props: any) {
    const [head, setHead] = useState(false);
    const nav = useNavigate()

    const changeBackground = () => {
        // console.log(window.scrollY)
        if (window.scrollY >= 80) {
            setHead(true)
        } else {
            setHead(false)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', changeBackground)
    }, [head]);

    const ToLogin = () => {
        nav('/login')
    }

    const ToRegister = () => {
        nav('/register')
    }

    const ToHome = () => {
        nav('/home')
    }

    const ToShowing = () => {
        nav('/movieshowing')
    }

    const ToComingSoon = () => {
        nav('/moviecomingsoon')
    }

    const ToSchedule = () => {
        nav('/schedule')
    }

    return (
        <div className={head ? 'head active' : 'head'}>
            <div className="mainSizeS m-a flex flex-spaceBtw flex-middle">
                <h1 onClick={ToHome}>Movie Booking App</h1>
                <ul className="flex">
                    <div className="dropdown">
                        <button className="dropbtn">Movies</button>
                        <div className="dropdown-content">
                            <li onClick={ToShowing} className="link">Showing</li>
                            <li onClick={ToComingSoon} className="link">Coming Soon</li>
                        </div>
                    </div>
                    <li onClick={ToSchedule} className="mr-30 link">Schedule</li>
                    <li onClick={ToLogin} className="mr-30 link">Login</li>
                    <li onClick={ToRegister} className="mr-30 link">Register</li>
                </ul>
            </div>
        </div>
    )
}
